import org.junit.jupiter.api.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OlderThanTest {

    // Создаем экземпляр SearchServiceImpl для тестирования
    private final SearchService searchService = new SearchServiceImpl();

    @Nested
    @DisplayName("Тесты для метода findOlderAnimal")
    class FindOlderAnimalTests {

        @ParameterizedTest(name = "Тест для значения = {0}")
        @ValueSource(ints = {1, 5, 10}) // Проверяем для разных значений возраста
        @DisplayName("Должен врзвращать имена животных старше определенного возраста")
        void testFindOlderAnimal(int age) {
            // Arrange
            List<Animal> animals = new ArrayList<>();
            LocalDate currentYear = LocalDate.now(); // Определяем год, который соответствует указанному возрасту

            animals.add(new Dog("Лабрадор", "Бип", 100.0, "Дружелюбный", currentYear.minusYears(age+2))); // Создаем животных разных возрастов
            animals.add(new Cat("Британец", "ЧИП", 80.0, "Ручной", currentYear.minusYears(age + 1)));
            animals.add(new Wolf("Серый", "Альф", 200.0, "Голодный", currentYear.minusYears(age - 1)));

            // Act
            List<Animal> olderAnimals = searchService.findOlderAnimal(animals, age);

            // Assert
            assertEquals(2, olderAnimals.size()); // Ожидаем, что будет возвращено два животных, так как только они старше заданного возраста
            assertTrue(olderAnimals.stream().allMatch(animal -> currentYear.getYear() - animal.getBirthDate().getYear() > age)); // Проверяем, что все возвращенные животные старше заданного возраста
        }

        @Test
        @DisplayName("Должен быть возвращен пустой массив если нет животных, старше определенного возраста")
        void testFindOlderAnimal_NoAnimalsOlderThanAge_ReturnsEmptyList() {
            // Arrange
            List<Animal> animals = new ArrayList<>();
            LocalDate currentYear = LocalDate.now();

            animals.add(new Dog("Лабрадор", "Бип", 100.0, "Дружелюбный", currentYear)); // Создаем животных младше указанного возраста

            // Act
            List<Animal> olderAnimals = searchService.findOlderAnimal(animals, 1);

            // Assert
            assertTrue(olderAnimals.isEmpty()); // Ожидаем, что список будет пустым, так как нет животных старше указанного возраста
        }
    }
}
