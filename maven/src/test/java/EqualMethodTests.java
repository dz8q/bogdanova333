import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Тестируем метод equals")
public class EqualMethodTests {

    @Nested
    @DisplayName("Тестирование равенства между параметрами животных одного типа")
    class EqualitySameType {

        @Test
        @DisplayName("Животные с одинаковой породой, именем, ценой и датой рождения должны быть одинаковы")
        void sameTypeSameAttributes() {
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            Dog dog1 = new Dog("Лабрадор", "Петр", 100.0, "Дружелюбный", birthDate);
            Dog dog2 = new Dog("Лабрадор", "Петр", 100.0, "Дружелюбный", birthDate);
            assertTrue(dog1.equals(dog2));
        }

        @Test
        @DisplayName("Двое животных с разными именами не должны быть одинаковы")
        void sameTypeDifferentName() {
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            Dog dog1 = new Dog("Лабрадор", "Петр", 100.0, "Дружелюбный", birthDate);
            Dog dog2 = new Dog("Лабрадор", "Рекс", 100.0, "Дружелюбный", birthDate);
            assertFalse(dog1.equals(dog2));
        }

        @Test
        @DisplayName("Двое животных с разными породами не должны быть одинаковы")
        void sameTypeDifferentBreed() {
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            Dog dog1 = new Dog("Лабрадор", "Петр", 100.0, "Дружелюбный", birthDate);
            Dog dog2 = new Dog("Гончая", "Петр", 100.0, "Дружелюбный", birthDate);
            assertFalse(dog1.equals(dog2));
        }
    }

    @Nested
    @DisplayName("Тестирование неравенства между животными разных типов")
    class InequalityDifferentTypes {

        @Test
        @DisplayName("Собака не равна кошке")
        void dogNotEqualCat() {
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            Dog dog = new Dog("Лабрадор", "Петр", 100.0, "Дружелюбный", birthDate);
            Cat cat = new Cat("Сиамский", "Вася", 80.0, "Когтистый", birthDate);
            assertFalse(dog.equals(cat));
        }

        @Test
        @DisplayName("Волк не равен акуле")
        void wolfNotEqualShark() {
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            Wolf wolf = new Wolf("Белый ", "Зубастый", 200.0, "Агрессивный", birthDate);
            Shark shark = new Shark("Белый ", "Зубастый", 200.0, "Агрессивный", birthDate);
            assertFalse(wolf.equals(shark));
        }
    }
}

