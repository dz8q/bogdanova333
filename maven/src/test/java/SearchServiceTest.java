import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Тестирование SearchService")
class SearchServiceTest {

    @Nested
    @DisplayName("Тестирование метода findLeapYearNames()")
    class FindLeapYearNamesTests {

        @Test
        @DisplayName("Должны быть возвращены имена животных родившихся в високосный год")
        void testFindLeapYearNames_ReturnsNamesOfAnimalsBornInLeapYears() {
            // Arrange
            List<Animal> animals = new ArrayList<>();
            animals.add(new Dog("Лабрадор", "Джек", 100.0, "Дружелюбный", LocalDate.of(2020, 1, 1)));
            animals.add(new Cat("Сиамский", "Кузя", 80.0, "Ленивый", LocalDate.of(2019, 1, 1))); // Не високосный
            animals.add(new Wolf("Серый", "Серый", 200.0, "Агрессивный", LocalDate.of(2024, 1, 1))); // Високосный
            List<LocalDate> birthdays = Arrays.asList(
                    LocalDate.of(2020, 1, 1),
                    LocalDate.of(2019, 1, 1),
                    LocalDate.of(2024, 1, 1)
            );
            SearchService searchService = new SearchServiceImpl();

            // Act
            List<String> leapYearNames = searchService.findLeapYearNames(animals, birthdays);

            // Assert
            assertEquals(2, leapYearNames.size()); // Ожидаем массив из двух элементов
            assertTrue(leapYearNames.contains("Джек"));
            assertTrue(leapYearNames.contains("Серый"));
        }

        @Test
        @DisplayName("Должен быть возвращен пустой массив если нет животных рожденных в високосный год")
        void testFindLeapYearNames_ReturnsEmptyListWhenNoAnimalsBornInLeapYears() {
            // Arrange
            List<Animal> animals = new ArrayList<>();
            animals.add(new Dog("Лабрадор", "Джек", 100.0, "Дружелюбный", LocalDate.of(2019, 1, 1)));
            List<LocalDate> birthdays = Arrays.asList(LocalDate.of(2019, 1, 1));
            SearchService searchService = new SearchServiceImpl();

            // Act
            List<String> leapYearNames = searchService.findLeapYearNames(animals, birthdays);

            // Assert
            assertTrue(leapYearNames.isEmpty());
        }


    }


}


