
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DuplicateTest {
    @Nested
    @DisplayName("Тесты для метода findDuplicate")
    class FindDuplicateTests {

        @Test
        @DisplayName("Возврат пустого массива если нет дубликатов")
        void testFindDuplicate_NoDuplicates_ReturnsEmptyList() {
            // Arrange
            SearchService searchService = new SearchServiceImpl();
            List<Animal> animals = new ArrayList<>();
            animals.add(new Dog("Корги", "Чапа", 100.0, "Громкий", LocalDate.of(2010, 1, 1)));
            animals.add(new Cat("МэйКун", "Чипи", 80.0, "Сонный", LocalDate.of(2011, 1, 1)));

            // Act
            List<Animal> duplicates = searchService.findDuplicate(animals);

            // Assert
            assertEquals(0, duplicates.size());
        }

        @Test
        @DisplayName("Возврат массива дубликатов при их присутствии")
        void testFindDuplicate_DuplicatesFound_ReturnsListOfDuplicates() {
            // Arrange
            SearchService searchService = new SearchServiceImpl();
            List<Animal> animals = new ArrayList<>();
            LocalDate birthDate = LocalDate.of(2010, 1, 1);
            animals.add(new Dog("Корги", "Чапа", 100.0, "Громкий", birthDate));
            animals.add(new Dog("Корги", "Чапа", 100.0, "Громкий", birthDate)); // Дубликат
            animals.add(new Cat("МэйКун", "Чипи", 80.0, "Сонный", LocalDate.of(2011, 1, 1)));
            animals.add(new Cat("МэйКун", "Чипи", 80.0, "Сонный", LocalDate.of(2012, 1, 1)));

            // Act
            List<Animal> duplicates = searchService.findDuplicate(animals);

            // Assert
            assertEquals(1, duplicates.size()); // Ожидаем, что найдется только один дубликат
        }

        @Test
        @DisplayName("При подаче на вход пустого списка вывод пустого массива")
        void testFindDuplicate_EmptyList_ReturnsEmptyList() {
            // Arrange
            SearchService searchService = new SearchServiceImpl();
            List<Animal> animals = new ArrayList<>();

            // Act
            List<Animal> duplicates = searchService.findDuplicate(animals);

            // Assert
            assertEquals(0, duplicates.size());
        }
    }
}



