import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


// Реализация SearchService
class SearchServiceImpl implements SearchService {
    @Override
    public List<String> findLeapYearNames(List<Animal> animals, List<LocalDate> birthdays) {
        return animals.stream()
                .filter(animal -> birthdays.get(animals.indexOf(animal)).isLeapYear())
                .map(Animal::getName)
                .peek(name -> System.out.println("Животное " + name + " родилось в високосный год"))
                .collect(Collectors.toList());
    }

    @Override
    public List<Animal> findOlderAnimal(List<Animal> animals, int age) {
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();

        return animals.stream()
                .filter(animal -> currentYear - animal.getBirthDate().getYear() > age)
                .peek(animal -> System.out.println("Животное " + animal.getName() + " старше чем " + age + " лет"))
                .collect(Collectors.toList());
    }

    @Override
    public List<Animal> findDuplicate(List<Animal> animals) {
        // Используем Stream API для преобразования списка животных в поток
        return animals.stream()
                // Группируем животных по их именам и датам рождения
                .collect(Collectors.groupingBy(animal -> Arrays.asList(animal.getName(), animal.getBirthDate())))
                // Оставляем только те группы, у которых более одного элемента (дубликаты)
                .entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                // Только 1 дубликат для каждой группы
                .map(entry -> entry.getValue().get(0))
                // Собираем результат обратно в список
                .collect(Collectors.toList());
    }
}
