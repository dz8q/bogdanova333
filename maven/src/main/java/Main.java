import java.time.LocalDate;
import java.util.*;

// Интерфейс Animal
interface Animal {
    String getBreed();
    String getName();
    double getCost();
    String getCharacter();
    LocalDate getBirthDate(); // Добавлено новое поле birthDate
}
// Интерфейс SearchService
interface SearchService {
    List<String> findLeapYearNames(List<Animal> animals, List<LocalDate> birthdays);
    List<Animal> findOlderAnimal(List<Animal> animals, int age);
    List<Animal> findDuplicate(List<Animal> animals);
}
// Абстрактный класс AbstractAnimal
abstract class AbstractAnimal implements Animal {
    protected String breed;
    protected String name;
    protected double cost;
    protected String character;
    protected LocalDate birthDate; // Добавлено новое поле birthDate

    // Конструктор
    public AbstractAnimal(String breed, String name, double cost, String character, LocalDate birthDate) {
        this.breed = breed;
        this.name = name;
        this.cost = cost;
        this.character = character;
        this.birthDate = birthDate;
    }

    // Геттеры
    @Override
    public String getBreed() {
        return breed;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public String getCharacter() {
        return character;
    }

    @Override
    public LocalDate getBirthDate() {
        return birthDate;
    }

    // Переопределение метода equals()
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractAnimal that = (AbstractAnimal) o;

        if (!breed.equals(that.breed)) return false;
        if (!name.equals(that.name)) return false;
        return birthDate.equals(that.birthDate);
    }
}

// Класс Pet
abstract class Pet extends AbstractAnimal {
    public Pet(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}

// Класс Predator
abstract class Predator extends AbstractAnimal {
    public Predator(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}

// Классы-наследники Pet
class Dog extends Pet {
    public Dog(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}

class Cat extends Pet {
    public Cat(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}

// Классы-наследники Predator
class Wolf extends Predator {
    public Wolf(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}

class Shark extends Predator {
    public Shark(String breed, String name, double cost, String character, LocalDate birthDate) {
        super(breed, name, cost, character, birthDate);
    }
}



public class Main {
    public static void main(String[] args) {
        // Создание объекта CreateServiceImpl
        CreateAnimalServiceImpl createService = new CreateAnimalServiceImpl();

        // Получение данных
        List<Animal> animals = new ArrayList<>(createService.createAnimals(200)); // Создаем 100 животных

        // Получение дат рождения из созданных животных
        List<LocalDate> birthDates = animals.stream()
                .map(Animal::getBirthDate)
                .toList();

        // Создание объекта SearchServiceImpl
        SearchServiceImpl searchService = new SearchServiceImpl();

        // Использование дат рождения для поиска животных, родившихся в високосные годы
        List<String> leapYearNames = searchService.findLeapYearNames(animals, birthDates);
        System.out.println("Животные родившиеся в високосный год:");
        leapYearNames.forEach(System.out::println);
// Поиск животных, старше 5 лет и вывод их имен
        List<Animal> olderAnimals = searchService.findOlderAnimal(animals, 5);
        System.out.println("\nЖивотные старше 5 лет:");
        olderAnimals.stream()
                .map(Animal::getName)
                .forEach(System.out::println);

        // Поиск дубликатов животных и вывод их имен
        List<Animal> duplicateAnimals = searchService.findDuplicate(animals);
        System.out.println("\nДубликаты:");
        duplicateAnimals.forEach(animal -> System.out.println(animal.getName() + " " + animal.getBirthDate()));
    }
}

