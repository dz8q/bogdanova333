import java.time.LocalDate;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;


class CreateAnimalService {
    protected Set<Animal> createdAnimals = new HashSet<>();

    // Метод для создания случайного животного
    protected Animal createRandomAnimal() {
        Random random = new Random();
        int choice = random.nextInt(4); // Генерируем случайное число от 0 до 3
        LocalDate birthDate = generateRandomBirthDate();

        return switch (choice) {
            case 0 -> new Dog("  Лабрадор ", "  Джек ", 100.0, "  Дружелюбный  ", birthDate);
            case 1 -> new Cat("  Сиамский ", " Вискас ", 80.0, "  Гордый  ", birthDate);
            case 2 -> new Wolf("  Полярный волк ", "  Сергей ", 200.0, "  Агрессивный  ", birthDate);
            case 3 -> new Shark("  Белая акула ", "  Мегаладон ", 500.0, "  Спокойный  ", birthDate);
            default ->
                // По умолчанию создаем собаку
                    new Dog("  Чихуахуа ", "  Принцесса", 100.0, "  Тревожный  ", birthDate);
        };
    }

    // Метод для генерации случайной даты рождения
    private LocalDate generateRandomBirthDate() {
        Random random = new Random();
        int year = random.nextInt(14) + 2010; // Случайный год от 2010 до 2023
        int month = random.nextInt(12) + 1; // Случайный месяц от 1 до 12
        int day = random.nextInt(28) + 1; // Случайный день от 1 до 28 (не високосный год)

        return LocalDate.of(year, month, day);
    }

    // Метод для создания 10 уникальных животных с использованием цикла while
    public void create10UniqueAnimals() {
        System.out.println("Creating 10 unique animals with while loop...");
        int count = 0;
        while (count < 10) {
            Animal animal = createRandomAnimal();
            if (!createdAnimals.contains(animal)) {
                createdAnimals.add(animal);
                System.out.println("Created: " + animal.getName() + animal.getBreed() + animal.getCharacter() + animal.getBirthDate());
                count++;
            }
        }
    }
}
class CreateAnimalServiceImpl extends CreateAnimalService {
    // Перегруженный метод для создания N животных с использованием цикла for
    public List<Animal> createAnimals(int n) {
        System.out.println("Создаем " + n + " животных с помощью цикла for...");
        List<Animal> animals = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Animal animal = createRandomAnimal();
            animals.add(animal);
            createdAnimals.add(animal);
            System.out.println("Созданы: " + animal.getName() + animal.getBreed()  + animal.getCharacter() + animal.getBirthDate());
        }
        return animals;
    }

    // Перегруженный метод для создания животных с заданными параметрами
    public void createAnimals(String breed, String name, double cost, String character, LocalDate birthDate) {
        Animal animal = null;
        if (breed.equals("Dog")) {
            animal = new Dog(breed, name, cost, character, birthDate);
        } else if (breed.equals("Cat")) {
            animal = new Cat(breed, name, cost, character, birthDate);
        } else if (breed.equals("Wolf")) {
            animal = new Wolf(breed, name, cost, character, birthDate);
        } else if (breed.equals("Shark")) {
            animal = new Shark(breed, name, cost, character, birthDate);
        }
        if (animal != null) {
            createdAnimals.add(animal);
            System.out.println("Created: " + animal.getName());
        }
    }

    // Переопределенный метод для создания 10 уникальных животных с использованием цикла do-while
    @Override
    public void create10UniqueAnimals() {
        System.out.println("Creating 10 unique animals with do-while loop...");
        int count = 0;
        do {
            Animal animal = createRandomAnimal();
            if (!createdAnimals.contains(animal)) {
                createdAnimals.add(animal);
                System.out.println("Created: " + animal.getName());


                count++;
            }
        } while (count < 10);
    }
}

