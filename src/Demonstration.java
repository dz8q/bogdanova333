import java.util.List;

public class Demonstration {
    public static void main(String[] args) { //Вызывать его будем с помощью функции main
        //Создаем объекты класса ItemInTheCart  и задаем их параметры
        ItemInTheCart item1 = new ItemInTheCart(5,45.5569,0.75);
        ItemInTheCart item2 = new ItemInTheCart(10,256.6669, 42.575);
        ItemInTheCart item3 = new ItemInTheCart(30,25.8188,51.9);
        List<Double> Recap1 = item1.priceAmounts(item1); //Вызываем метод priceAmounts обращаясь к объекту item1
        //(а не к классу как было при использовании static, т.к если метод не static, то он не может быть вызыван пока не создан объект)
        for (int i=0; i<2; i++){ //С помощью цикла for выводим массив-строку с результатами для объекта item1
            System.out.println(Recap1.get(i));
        }
        List<Double> Recap2 = item2.priceAmounts(item2);//С помощью цикла for выводим массив-строку для объекта item2
        for (int i=0; i<2; i++){
            System.out.println(Recap2.get(i));
        }
        List<Double> Recap3 = item3.priceAmounts(item3);//С помощью цикла for выводим массив-строку для объекта item3
        for (int i=0; i<2; i++){
            System.out.println(Recap3.get(i));
        }
    }
}