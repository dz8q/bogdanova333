package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
public class RegistrationPage {

    private WebDriver driver;
    private WebDriverWait wait;
    public By popupTitle1 = By.xpath("//button[contains(text(),'Закрыть')]");
    public By popupTitle2 = By.xpath("//a[contains(text(),'На главную')]");

    // Локаторы элементов страницы
    private By nameInput = By.name("name");
    private By emailInput = By.name("email");
    private By phoneInput = By.name("tel");
    private By passwordInput = By.name("password1");
    private By confirmPasswordInput = By.name("password2");
    private By registerButton = By.xpath("//button[contains(text(), 'Зарегистрироваться')]");
    private By acceptCookiesButton = By.cssSelector(".cookie__btn.btn.js-accept-cookies");

    // Конструктор класса
    public RegistrationPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10)); // 10 секунд ожидания
    }
    public String getPopupTitle(By locator) {
        try {
            Thread.sleep(10000); // Пауза
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            return driver.findElement(locator).getText();
    }
    // Методы для взаимодействия с элементами страницы
    public void setName(String name) {
        WebElement nameField = driver.findElement(nameInput);
        nameField.clear();
        nameField.sendKeys(name);
    }

    public void setEmail(String email) {
        WebElement emailField = driver.findElement(emailInput);
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void setPhone(String phone) {
        WebElement phoneField = driver.findElement(phoneInput);
        phoneField.clear();
        phoneField.sendKeys(phone);
    }

    public void setPassword(String password) {
        WebElement passwordField = driver.findElement(passwordInput);
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void setConfirmPassword(String password) {
        WebElement confirmPasswordField = driver.findElement(confirmPasswordInput);
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(password);
    }
    public void acceptCookies() {
        WebElement acceptCookiesButtonElement = wait.until(ExpectedConditions.elementToBeClickable(acceptCookiesButton));
        acceptCookiesButtonElement.click();
    }
    public void clickRegisterButton() {
        acceptCookies();
        driver.findElement(registerButton).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    }
    public void clickRegisterButton1() {
        driver.findElement(registerButton).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    }
    public void registerNewUser(String name, String email, String phone, String password) {
        setName(name);
        setEmail(email);
        setPhone(phone);
        setPassword(password);
        setConfirmPassword(password);
        clickRegisterButton();
    }
    public void registerNewUser2(String name, String email, String phone, String password) {
        setName(name);
        setEmail(email);
        setPhone(phone);
        setPassword(password);
        setConfirmPassword(password);
        clickRegisterButton1();
    }
    // Метод для проверки видимости попапа
    public boolean isPopupNotVisible(By locator) {
        try {
            // Попытка ожидания появления элемента
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return false; // Попап был найден
        } catch (TimeoutException e) {
            return true; // Попап не был найден
        }
    }
}







