import java.util.List;
/*
Создаем класс ItemInTheCart (товар в корзине), характеризующийся тремя параметрами.
Целочисленный параметр quantity- количество товара, вещественный параметрт price- цена за один товар
вещественный параметр discount- скидка в % на данный товар. Каждому объекту в классе ItemInTheCart
присваиваются эти параметры.
 */
public class ItemInTheCart {
    int quantity; //Количество товара
    double price; //Цена за штуку
    double discount; //Скидка
    public ItemInTheCart(int _quantity, double _price, double _discount){
        this.quantity = _quantity;
        this.price = _price;
        this.discount = _discount;
    }//конструктор
    //Создаем метод, возвращающий после своего выполнения массив-строку из двух значений (сумма со скидкой и без скидки)
    public List<Double> priceAmounts (ItemInTheCart item){ /*Параметром для метода является объект
       класса ItemInTheCart
       */
        double amountWoutDiskount=0,amountWithDiskount=0; //Обнуляем счетчики сумм
        for(int i=0; i<item.quantity; i=i+1) { //Используем цикл for для более удобного счета
            amountWoutDiskount=amountWoutDiskount+item.price;amountWithDiskount=amountWithDiskount+item.price*(100-item.discount)/100;
        }//Суммируем цену товаров без скидки и со скидкой
        return List.of((Math.round( amountWoutDiskount*100.0)/100.0),(Math.round(amountWithDiskount*100.0)/100.0));
        //Метод возвращает уже округленные значения сумм
    }
}
