package steps;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.ru.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.RegistrationPage;
import static org.junit.Assert.*;
public class RegistrationSteps {

    private static WebDriver driver;
    private RegistrationPage registrationPage;

    @BeforeAll
    public static void setUp() {
        WebDriverManager.chromedriver().clearDriverCache().setup();
        driver = new ChromeDriver();
    }

    @AfterAll
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Дано("Пользователь находится на странице регистрации")
    public void пользователь_находится_на_странице_регистрации() {
        driver.get("https://frutonyanya.ru/register/");
        registrationPage = new RegistrationPage(driver);
    }

    @Когда("Пользователь заполняет форму регистрации с {string} {string} {string} {string}")
    public void пользователь_заполняет_форму_регистрации(String name, String email, String phone, String password) {
        // Генерация уникальной почты для положительного теста
        String uniqueEmail = System.currentTimeMillis()+email;
        // Регистрация нового пользователя с уникальной почтой
        registrationPage.registerNewUser(name, uniqueEmail, phone, password);
    }

    @Тогда("Появляется попап с заголовком {string}")
    public void появляется_попап_с_заголовком(String expectedTitle) {
        assertEquals(expectedTitle, registrationPage.getPopupTitle(registrationPage.popupTitle2));

    }

    @Когда("Пользователь заполняет форму регистрации с неправильными данными")
    public void пользователь_заполняет_форму_регистрации_с_неправильными_данными() {
        // Генерация уникальной почты для негативного теста
        String uniqueEmail = "@invalid@";  // Указываем неправильный формат электронной почты
        // Регистрация нового пользователя с неправильной почтой
        registrationPage.registerNewUser2("User", uniqueEmail, "+70000000000", "password123");
    }

    @Тогда("Попап с сообщением не появляется")
    public void попап_не_появляется() {
        assertTrue("Попап появился",registrationPage.isPopupNotVisible(registrationPage.popupTitle1) || registrationPage.isPopupNotVisible(registrationPage.popupTitle2));
    }

}
