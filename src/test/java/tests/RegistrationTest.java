package tests;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.RegistrationPage;

import static org.junit.Assert.*;

public class RegistrationTest {

    private static WebDriver driver;
    public RegistrationPage registrationPage;

    @Before
    public void setUp() {

        WebDriverManager.chromedriver().clearDriverCache().setup();
        driver = new ChromeDriver();
        driver.get("https://frutonyanya.ru/register/");
        registrationPage = new RegistrationPage(driver);
    }

    @Test
    public void testPositiveRegistration() {
        // Генерация уникальной почты для положительного теста
        String email = "test" + System.currentTimeMillis() + "@example.com";
        // Регистрация нового пользователя с уникальной почтой
        registrationPage.registerNewUser("User", email, "+70000000000", "password123");
        // Проверяем, что попап с заголовком "Спасибо!" появился
        assertEquals("На главную", registrationPage.getPopupTitle(registrationPage.popupTitle2));
    }

    @Test
    public void testNegativeRegistration() {
        // Регистрация нового пользователя с теми же данными
        registrationPage.registerNewUser("User", "1valid", "+70000000000", "password123");
        // Проверка отсутствия попапа с сообщением об ошибке
        assertTrue("Попап появился", registrationPage.isPopupNotVisible(registrationPage.popupTitle2) && registrationPage.isPopupNotVisible(registrationPage.popupTitle1));
    }
    @After
    public void tearDown() {
        // Закрываем браузер после завершения теста
        if (driver != null) {
            driver.quit();
        }
    }
}